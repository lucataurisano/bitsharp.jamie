﻿using System;
using Bitsharp.Jamie.Logger;
using log4net;

namespace Bitsharp.Logger.Logger
{
    public class Logger<T> : ILogger<T>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(T));
        public void LogInfo(string message)
        {
            log.Info(message);
        }

        public void LogError(string message, Exception ex)
        {
            log.Error(message,ex);
        }

        public void LogWarning(string message)
        {
            log.Warn(message);
        }
    }
}