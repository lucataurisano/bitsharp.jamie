﻿using System;

namespace Bitsharp.Jamie.Logger
{
    public interface ILogger<T>
    {
        void LogInfo(string message);
        void LogError(string message,Exception ex);
        void LogWarning(string message);
    }
}
