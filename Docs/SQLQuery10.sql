--select cp.duedate, p.*, cp.duedate,p.posid --,per.name,per.email, p.*
select 
cp.duedate,
cp.assignedtocode ,
pos.ownerpersoncode as PositionName,
--per.name as PositionName, 
--per.email as PositionMail, 
perCp.name as ProcedureName,
perCp.email as ProcedureMail,
pos.posid ,
pos.Name as positnName,
f.Name as FunctionName, 
cp.name as CallProcedureName,f.*,cp.*
from calprocedure cp
inner join func f on cp.funccode = f.funccode
inner join position pos on f.poscode = pos.poscode
--inner join person per on pos.ownerpersoncode = per.code
inner join person perCp on cp.assignedtocode = perCp.code
where cp.duedate between getdate() and '2019-10-31'
order by cp.duedate desc; --pos.posid;

select cp.duedate,per.name,per.email, p.*, cp.duedate,p.posid , p.*
from calprocedure cp
inner join func f on cp.funccode = f.funccode
inner join position p on f.poscode = p.poscode
inner join person per on p.ownerpersoncode = per.code
order by p.posid;

select cp.duedate, per.name, per.email,pos.ownerpersoncode,pos.posid ,pos.Name as positnName,f.Name as FunctionName, cp.name as CallProcedureName,f.*,cp.*
from calprocedure cp
inner join func f on cp.funccode = f.funccode
inner join position pos on f.poscode = pos.poscode
left join person per on pos.ownerpersoncode = per.code
order by cp.duedate desc-- pos.posid,f.Name ;

select *
from calprocedure
order by name;

select *
from func;

select *
from position;

select *
from person
order by code;

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT *   FROM [CMX_Demo_Database].[dbo].[PLANTSTRUCTURE]