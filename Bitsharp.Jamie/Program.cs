﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Bitsharp.JamieService;
using Bitsharp.JamieService.Logger;

namespace Bitsharp.Jamie
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            try
            {
                Setup();

                //if (CheckKey())
                //{
                //    BasicLogger.Log("Chiave di attivazione valida");
                ServiceBase.Run(new JamieService());
                //}
                //else
                //{
                //    throw new ApplicationException("Chiave di attivazione non valida!");
                //}
            }
            catch (Exception ex)
            {
                BasicLogger.Log("Program.Main", ex);
                throw;
            }
        }

        static bool CheckKey()
        {
            return JamieActivationKeyChecker.CheckKey();
        }

        static void Setup()
        {
            JamieServiceConfiguration.Setup();
        }
    }
}
