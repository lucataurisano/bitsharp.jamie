﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bitsharp.Jamie.Dal;
using Bitsharp.JamieService;

namespace Bitsharp.Jamie
{
    public static class JamieServiceConfiguration
    {
        public static void Setup()
        {
            LoggingConfiguration.Setup();
            JamieDependencyConfiguration.Setup();
            JamieDalConfiguration.Setup();
        }
    }
}
