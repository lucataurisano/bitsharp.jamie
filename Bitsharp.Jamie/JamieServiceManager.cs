﻿using System;
using System.Threading.Tasks;
using System.Timers;
using Bitsharp.Jamie;
using Bitsharp.Jamie.Application;
using Bitsharp.Jamie.Logger;

namespace Bitsharp.JamieService
{
    public class JamieServiceManager
    {
        private static JamieServiceManager serviceManager;
        private readonly ILogger<JamieServiceManager> logger;
        private static IJamieServiceConfigurationManager configuration;
        private readonly ICalProcedureService procedureService;

        private static Task task;
        Timer timer = new Timer();

        private JamieServiceManager()
        {
            logger = JamieDependencyConfiguration.Resolve<ILogger<JamieServiceManager>>();
            configuration = JamieDependencyConfiguration.Resolve<IJamieServiceConfigurationManager>();
            procedureService = JamieDependencyConfiguration.Resolve<ICalProcedureService>();
            JamieTaskSetup();
        }

        public static JamieServiceManager Instance()
        {
            return serviceManager ?? (serviceManager = new JamieServiceManager());
        }

        public void JamieTaskSetup()
        {
            var scheduler = configuration.GetJamieServiceSchedulerHourlyInterval();
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = TimeSpan.FromHours(scheduler).TotalMilliseconds; ; //number in milisecinds  
            timer.Enabled = true;
            logger.LogInfo(string.Format("JamieTaskSetup() every scheduler days: [{0}]", scheduler));
        }

        public void StartMonitoring()
        {
            try
            {
                var daysBeforeCheck = configuration.GetJamieServiceDaysBeforeCheck();
                logger.LogInfo(string.Format("StartMonitoring() at [{0}]", DateTime.Now.ToLongDateString()));

                var dateBeforeCheck = DateTime.Now.AddDays(daysBeforeCheck);
                procedureService.SendMailForCalProcedureExpired(dateBeforeCheck);
            }
            catch (Exception ex)
            {
                logger.LogError("StartMonitoring error", ex);
                throw;
            }
        }

        public void StopMonitoring()
        {
            try
            {
                logger.LogInfo(string.Format("StopMonitoring() at [{0}]", DateTime.Now.ToLongDateString()));
                task.Dispose();
            }
            catch (Exception ex)
            {
                logger.LogError("StopMonitoring error", ex);
                throw;
            }
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            try
            {
                logger.LogInfo("Start task  OnElapsedTime()");
                task = new Task(StartMonitoring);
                task.Start();
            }
            catch (Exception ex)
            {
                logger.LogError("ProcessProcedure", ex);
                throw;
            }
        }

        private void SendMail()
        {

        }
    }
}
