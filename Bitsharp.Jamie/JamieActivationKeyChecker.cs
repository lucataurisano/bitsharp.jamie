﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Bitsharp.Jamie;

namespace Bitsharp.JamieService
{
    public class JamieActivationKeyChecker
    {
        public static bool CheckKey()
        {
            String firstMacAddress = NetworkInterface
                .GetAllNetworkInterfaces()
                .Where(nic => nic.OperationalStatus == OperationalStatus.Up && nic.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                .Select(nic => nic.GetPhysicalAddress().ToString())
                .FirstOrDefault();

            IJamieServiceConfigurationManager jamieServiceConfigurationManager = 
                JamieDependencyConfiguration.Resolve<JamieServiceConfigurationManager>();
            var activationCode = jamieServiceConfigurationManager.GetJamieServiceActivatorKey();

            var activationArray = activationCode.Split('-');
            var activationCodePublic = activationArray[0];
            var activationCodePrivate = activationArray[1];
            Guid g = new Guid(activationCodePublic);
            var validHashCode = g.GetHashCode().ToString().Substring(1);


            return VerifyMd5Hash(firstMacAddress, activationCodePublic) && activationCodePrivate == validHashCode;
        }


        private static bool VerifyMd5Hash(string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static string GetMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
    }
}
