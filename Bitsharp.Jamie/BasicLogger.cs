﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bitsharp.JamieService.Logger
{
    internal static class BasicLogger
    {
        const string logFile = "Bitsharp.Jamie.JamieService.log";

        static string GetAssemblyDirectory()
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var uri = new UriBuilder(codeBase);
            var path = Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));

            return path;
        }

        static string GetLogFile()
        {
            return Path.Combine(GetAssemblyDirectory(), logFile);
        }

        public static void Log(string message)
        {
            File.AppendAllText(GetLogFile(), string.Format("\r\n{0}: \r\n{1} \r\n", DateTime.Now.ToString("dd/MM/yyyy HH:mm"), message));
        }

        public static void Log(string message, Exception ex)
        {
            File.AppendAllText(GetLogFile(), string.Format("\r\n{0}: \r\n{1} \r\n{2} \r\n", DateTime.Now.ToString("dd/MM/yyyy HH:mm"), message, ex));
        }

        public static void Log(Exception ex)
        {
            File.AppendAllText(GetLogFile(), string.Format("\r\n{0}: \r\n{1} \r\n", DateTime.Now.ToString("dd/MM/yyyy HH:mm"), ex));
        }
    }
}
