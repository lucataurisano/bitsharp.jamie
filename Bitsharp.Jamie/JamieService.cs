﻿using System;
using System.Linq;
using System.ServiceProcess;
using Bitsharp.Jamie.Application.Ports;
using Bitsharp.Jamie.Dal;
using Bitsharp.Jamie.Logger;
using Bitsharp.JamieService;
using Bitsharp.Smtp.Smtp;

namespace Bitsharp.Jamie
{
    public partial class JamieService : ServiceBase
    {
        private readonly ILogger<JamieService> logger;
        private readonly IQueryService queryService;
        private static IJamieServiceConfigurationManager configuration;
        private readonly ISmtp smtp;

        public JamieService()
        {
            InitializeComponent();

            logger = JamieDependencyConfiguration.Resolve<ILogger<JamieService>>();
            queryService = JamieDependencyConfiguration.Resolve<IQueryService>();
            configuration = JamieDependencyConfiguration.Resolve<IJamieServiceConfigurationManager>();
            smtp = JamieDependencyConfiguration.Resolve<ISmtp>();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                logger.LogInfo("JamieService.OnStart()");
                var person = queryService.Persons().OrderByDescending(ord => ord.Name).FirstOrDefault();
                var message = string.Format("Connecction to database successfully! Person name [{0}] and code [{1}]", person.Name, person.Code);
                logger.LogInfo(message);
                //sendEmail("chiamata a JamieService.OnStart()", "[JAMIE ALERT] - Service Jamie STOP");
                var host = configuration.GetJamieServiceMailSmtpHost();
                var username = configuration.GetJamieServiceMailUsernameHost();
                var password = configuration.GetJamieServiceMailPasswordHost();
                var port = configuration.GetJamieServiceMailPort();
                var from = configuration.GetJamieServiceMailFromHost();
                var to = configuration.GetJamieServiceMailToHost();
                var fromDisplay = configuration.GetJamieServiceMailDisplayFromHost();
                var toDisplay = configuration.GetJamieServiceMailDisplayToHost();
                var body = configuration.GetJamieServiceMailBodyHost();
                var enableSsl = configuration.GetJamieServiceMailEnableSsl();

                var daysBeforeCheck = configuration.GetJamieServiceDaysBeforeCheck();

                logger.LogInfo(string.Format("StartMonitoring() at [{0}]", DateTime.Now.ToLongDateString()));
                smtp.Send(
                    from,
                    to,
                    fromDisplay,
                    toDisplay,
                    "Test del servizio mail!",
                    "Il servizio mail funziona!",
                    host,
                    username,
                    password,
                    port,
                    enableSsl);

                JamieServiceManager.Instance().StartMonitoring();
            }
            catch (Exception ex)
            {
                logger.LogError("JamieService.OnStart()", ex);
            }
           
        }

        protected override void OnStop()
        {
            try
            {
                logger.LogInfo("JamieService.OnStop()");
                //sendEmail("chiamata a JamieService.OnStop()", "[JAMIE ALERT] - Service Jamie STOP");

                JamieServiceManager.Instance().StopMonitoring();
            }
            catch (Exception ex)
            {
                logger.LogError("JamieService.OnStop()", ex);
                throw;
            }
        }
    }
}
