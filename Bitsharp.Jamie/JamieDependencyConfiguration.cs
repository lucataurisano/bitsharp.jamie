﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Bitsharp.Jamie.Application;
using Bitsharp.Jamie.Application.Ports;
using Bitsharp.Jamie.Dal;
using Bitsharp.Jamie.Dal.Adapters;
using Bitsharp.Jamie.Logger;
using Bitsharp.JamieService;
using Bitsharp.Logger.Logger;
using Bitsharp.Smtp.Smtp;
using Ninject;

namespace Bitsharp.Jamie
{
    public class JamieDependencyConfiguration
    {
        private static JamieDependencyConfiguration jamieDependencyConfiguration;
        private static IKernel kernel;

        public static JamieDependencyConfiguration Instance()
        {
            return jamieDependencyConfiguration ?? (jamieDependencyConfiguration = new JamieDependencyConfiguration());
        }

        public static void Setup()
        {
            kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());

            kernel.Bind<ILogger<QueryService>>().To<Logger<QueryService>>();
            kernel.Bind<ILogger<JamieService>>().To<Logger<JamieService>>();
            kernel.Bind<ILogger<JamieServiceManager>>().To<Logger<JamieServiceManager>>();
            kernel.Bind<ILogger<CalProcedureService>>().To<Logger<CalProcedureService>>();
            kernel.Bind<ILogger<Smtp.Smtp.Smtp>>().To<Logger<Smtp.Smtp.Smtp>>();
            kernel.Bind<ILogger<JamieServiceConfigurationManager>>().To<Logger<JamieServiceConfigurationManager>>();
            kernel.Bind<ILogger<JamieConfigurationManager>>().To<Logger<JamieConfigurationManager>>();


            kernel.Bind<IQueryService>().To<QueryService>();
            kernel.Bind<IJamieServiceConfigurationManager>().To<JamieServiceConfigurationManager>();
            kernel.Bind<IJamieConfigurationManager>().To<JamieConfigurationManager>();
            kernel.Bind<ISmtp>().To<Smtp.Smtp.Smtp>();
            kernel.Bind<ICalProcedureService>().To<CalProcedureService>();

            kernel.Bind<DbContext>().To<JamieDbContext>();

        }

        public static T Resolve<T>() where T : class
        {
            return kernel.Get<T>();
        }
    }
}
