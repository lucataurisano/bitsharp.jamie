﻿using System;
using System.Configuration;
using Bitsharp.Jamie.Logger;

namespace Bitsharp.JamieService
{
    public class JamieServiceConfigurationManager : IJamieServiceConfigurationManager
    {
        const string JamieServiceSchedulerHourlyIntervalKey = "JamieService.SchedulerHourlyInterval";
        const string JamieServiceDaysBeforeCheck = "JamieService.JamieServiceDaysBeforeCheck";
        const string JamieServiceeMailSmtpHost = "JamieService.JamieServiceeMailSmtpHost";
        const string JamieServiceMailUsernameHost = "JamieService.JamieServiceMailUsernameHost";
        const string JamieServiceMailPasswordHost = "JamieService.JamieServiceMailPasswordHost";
        const string JamieServiceMailBodyHost = "JamieService.JamieServiceMailBodyHost";
        const string JamieServiceMailFromHost = "JamieService.JamieServiceMailFromHost";
        const string JamieServiceMailToHost = "JamieService.JamieServiceMailToHost";
        const string JamieServiceMailDisplayFromHost = "JamieService.JamieServiceMailDisplayFromHost";
        const string JamieServiceMailDisplayToHost = "JamieService.JamieServiceMailDisplayToHost";
        const string JamieServiceMailPort = "JamieService.JamieServiceMailPort";
        const string JamieServiceMailEnableSsl = "JamieService.JamieServiceMailEnableSsl";

        const string JamieServiceMailPositionMailSubject = "JamieService.JamieServiceMailPositionMailSubject";
        const string JamieServiceMailProcedureMailSubject = "JamieService.JamieServiceMailProcedureMailSubject";


        const string JamieServiceActivatorKey = "JamieService.JamieServiceActivatorKey";

        private ILogger<JamieServiceConfigurationManager> logger;

        public JamieServiceConfigurationManager(ILogger<JamieServiceConfigurationManager> logger)
        {
            this.logger = logger;
        }

        public int GetJamieServiceSchedulerHourlyInterval()
        {
            var schedulerHourlyInterval = GetValueFromKey(JamieServiceSchedulerHourlyIntervalKey);
            int value;
            if (!int.TryParse(schedulerHourlyInterval, out value))
                throw new InvalidCastException(string.Format("invalid value for AppSettings key [{0}]", JamieServiceSchedulerHourlyIntervalKey));

            return value;
        }

        public int GetJamieServiceDaysBeforeCheck()
        {
            var schedulerDayBeforeCheck = GetValueFromKey(JamieServiceDaysBeforeCheck);
            int value;
            if (!int.TryParse(schedulerDayBeforeCheck, out value))
                throw new InvalidCastException(string.Format("invalid value for AppSettings key [{0}]", JamieServiceDaysBeforeCheck));

            return value;
        }

        public string GetJamieServiceMailSmtpHost()
        {
            var jamieServiceeMailSmtpHost = GetValueFromKey(JamieServiceeMailSmtpHost);
            if (string.IsNullOrEmpty(jamieServiceeMailSmtpHost))
                throw new InvalidCastException(string.Format("invalid value for AppSettings key [{0}]",JamieServiceeMailSmtpHost));

            return jamieServiceeMailSmtpHost;
        }

        public string GetJamieServiceMailUsernameHost()
        {
            var jamieServiceMailUsernameHost = GetValueFromKey(JamieServiceMailUsernameHost);
            if (string.IsNullOrEmpty(jamieServiceMailUsernameHost))
                throw new InvalidCastException(string.Format("invalid value for AppSettings key [{0}]", JamieServiceMailUsernameHost));

            return jamieServiceMailUsernameHost;
        }

        public string GetJamieServiceMailPasswordHost()
        {
            var jamieServiceMailPasswordHost = GetValueFromKey(JamieServiceMailPasswordHost);
            if (string.IsNullOrEmpty(jamieServiceMailPasswordHost))
                throw new InvalidCastException(string.Format("invalid value for AppSettings key [{0}]", JamieServiceMailPasswordHost));

            return jamieServiceMailPasswordHost;
        }

        public string GetJamieServiceMailBodyHost()
        {
            var jamieServiceMailBodyHost = GetValueFromKey(JamieServiceMailBodyHost);
            if (string.IsNullOrEmpty(jamieServiceMailBodyHost))
                throw new InvalidCastException(string.Format("invalid value for AppSettings key [{0}]", JamieServiceMailBodyHost));

            return jamieServiceMailBodyHost;
        }

        public string GetJamieServiceMailFromHost()
        {
            logger.LogInfo(JamieServiceMailFromHost);
            var jamieServiceMailFromHost = GetValueFromKey(JamieServiceMailFromHost);
            logger.LogInfo(jamieServiceMailFromHost);

            if (string.IsNullOrEmpty(jamieServiceMailFromHost))
                throw new InvalidCastException(string.Format("invalid value for AppSettings key [{0}]", JamieServiceMailFromHost));

            return jamieServiceMailFromHost;
        }

        public string GetJamieServiceMailToHost()
        {
            var jamieServiceMailToHost = GetValueFromKey(JamieServiceMailToHost);
            if (string.IsNullOrEmpty(jamieServiceMailToHost))
                throw new InvalidCastException(string.Format("invalid value for AppSettings key [{0}]", JamieServiceMailToHost));

            return jamieServiceMailToHost;
        }

        public string GetJamieServiceMailDisplayFromHost()
        {
            var jamieServiceMailDisplayFromHost = GetValueFromKey(JamieServiceMailDisplayFromHost);
            if (string.IsNullOrEmpty(jamieServiceMailDisplayFromHost))
                throw new InvalidCastException(string.Format("invalid value for AppSettings key [{0}]", JamieServiceMailDisplayFromHost));

            return jamieServiceMailDisplayFromHost;
        }

        public string GetJamieServiceMailDisplayToHost()
        {
            var jamieServiceMailDisplayToHost = GetValueFromKey(JamieServiceMailDisplayToHost);
            if (string.IsNullOrEmpty(jamieServiceMailDisplayToHost))
                throw new InvalidCastException(string.Format("invalid value for AppSettings key [{0}]", JamieServiceMailDisplayToHost));

            return jamieServiceMailDisplayToHost;
        }

        public int GetJamieServiceMailPort()
        {
            var jamieServiceMailPort = GetValueFromKey(JamieServiceMailPort);
            int value;
            if (!int.TryParse(jamieServiceMailPort, out value))
                throw new InvalidCastException(string.Format("invalid value for AppSettings key [{0}]", JamieServiceMailPort));

            return value;
        }

        public bool GetJamieServiceMailEnableSsl()
        {
            var jamieServiceMailEnableSsl = GetValueFromKey(JamieServiceMailEnableSsl);
            bool value;
            if (!bool.TryParse(jamieServiceMailEnableSsl, out value))
                throw new InvalidCastException(string.Format("invalid value for AppSettings key [{0}]", JamieServiceMailEnableSsl));

            return value;
        }

        public string GetJamieServiceActivatorKey()
        {
            logger.LogInfo(JamieServiceActivatorKey);
            var jamieServiceActivatorKey = GetValueFromKey(JamieServiceActivatorKey);
            logger.LogInfo(jamieServiceActivatorKey);

            if (string.IsNullOrEmpty(jamieServiceActivatorKey))
                throw new InvalidCastException(string.Format("invalid value for AppSettings key [{0}]", JamieServiceActivatorKey));

            return jamieServiceActivatorKey;
        }

        public string GetJamieServiceMailPositionMailSubject()
        {
            var jamieServiceMailPositionMailSubject = GetValueFromKey(JamieServiceMailPositionMailSubject);
            if (string.IsNullOrEmpty(jamieServiceMailPositionMailSubject))
                throw new InvalidCastException(string.Format("invalid value for AppSettings key [{0}]", JamieServiceMailPositionMailSubject));

            return jamieServiceMailPositionMailSubject;
        }

        public string GetJamieServiceMailProcedureMailSubject()
        {
            var jamieServiceMailProcedureMailSubject = GetValueFromKey(JamieServiceMailProcedureMailSubject);
            if (string.IsNullOrEmpty(jamieServiceMailProcedureMailSubject))
                throw new InvalidCastException(string.Format("invalid value for AppSettings key [{0}]", JamieServiceMailProcedureMailSubject));

            return jamieServiceMailProcedureMailSubject;
        }

        static string GetValueFromKey(string key)
        {
            var value = ConfigurationManager.AppSettings.Get(key);
            if (string.IsNullOrWhiteSpace(value)) throw new ConfigurationErrorsException(string.Format("Missing configuration setting for key [{0}]", key));

            return value;
        }
    }
}
