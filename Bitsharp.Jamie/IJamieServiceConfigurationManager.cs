﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitsharp.JamieService
{
    public interface IJamieServiceConfigurationManager
    {
        int GetJamieServiceSchedulerHourlyInterval();
        int GetJamieServiceDaysBeforeCheck();

        string GetJamieServiceMailSmtpHost();
        string GetJamieServiceMailUsernameHost();
        string GetJamieServiceMailPasswordHost();
        string GetJamieServiceMailBodyHost();
        string GetJamieServiceMailFromHost();
        string GetJamieServiceMailToHost();
        string GetJamieServiceMailDisplayFromHost();
        string GetJamieServiceMailDisplayToHost();
        int GetJamieServiceMailPort();
        bool GetJamieServiceMailEnableSsl();
        string GetJamieServiceActivatorKey();

        string GetJamieServiceMailPositionMailSubject();
        string GetJamieServiceMailProcedureMailSubject();
    }
}
