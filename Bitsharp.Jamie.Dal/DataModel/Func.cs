﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Bitsharp.Jamie.Dal.DataModel
{
    public class Func
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FuncCode { get; set; }
        public string Name { get; set; }
        public int PosCode { get; set; }
    }
}
