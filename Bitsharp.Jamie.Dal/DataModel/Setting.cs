﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Bitsharp.Jamie.Dal.DataModel
{
    public class Setting
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public string Data { get; set; }
        public int PGrCode { get; set; }
    }
}
