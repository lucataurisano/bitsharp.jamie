﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Bitsharp.Jamie.Dal.DataModel
{
    public class Position
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PosCode { get; set; }
        public string PosId { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public int OwnerPersonCode { get; set; }

    }
}
