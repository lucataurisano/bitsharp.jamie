﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Bitsharp.Jamie.Dal.DataModel
{
    public class Person
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Code { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

    }
}
