﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bitsharp.Jamie.Dal.DataModel
{
    public class CalProcedure
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Code { get; set; }
        public int? Assignedtocode { get; set; }
        public string Name { get; set; }
        public int IsActive { get; set; }
        public DateTime? Duedate { get; set; }
        public int FuncCode { get; set; }
    }
}
