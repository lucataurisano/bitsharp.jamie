﻿namespace Bitsharp.Jamie.Dal.Adapters
{
    public interface IJamieConfigurationManager
    {
        string GetJamieServiceMailSmtpHost();
        string GetJamieServiceMailUsernameHost();
        string GetJamieServiceMailPasswordHost();
        string GetJamieServiceMailBodyHost();
        string GetJamieServiceMailFromHost();
        string GetJamieServiceMailToHost();
        string GetJamieServiceMailDisplayFromHost();
        string GetJamieServiceMailDisplayToHost();
        int GetJamieServiceMailPort();
        bool GetJamieServiceMailEnableSsl();

        string GetJamieServiceMailPositionMailSubject();
        string GetJamieServiceMailProcedureMailSubject();
    }
}
