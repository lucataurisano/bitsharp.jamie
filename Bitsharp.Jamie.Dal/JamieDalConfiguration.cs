﻿using System;
using System.Configuration;

namespace Bitsharp.Jamie.Dal
{
    public static class JamieDalConfiguration
    {
        public static void Setup()
        {
            var connectionStringSettings = ConfigurationManager.ConnectionStrings["Jamie"];
            if (connectionStringSettings == null)
            {
                throw new InvalidOperationException("Missing [Jamie] connection string in configuration file");
            }
            var connectionString = connectionStringSettings.ConnectionString;
            Setup(connectionString);
        }

        public static void Setup(string connectionString)
        {
            JamieDbContext.ConnectionString = connectionString;

            InternalSetup();
        }

        private static void InternalSetup()
        {

        }
    }
}
