﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bitsharp.Jamie.Dal.DataModel;

namespace Bitsharp.Jamie.Dal
{
    public sealed class JamieDbContext : DbContext
    {
        public static string ConnectionString { get; set; }
        static JamieDbContext()
        {
            Database.SetInitializer<JamieDbContext>(null); // Disable EF Migrations
        }

        public JamieDbContext() : base(ConnectionString) { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Person>()
                .HasKey(t => t.Code)
                .ToTable("PERSON");

            modelBuilder.Entity<CalProcedure>()
                .HasKey(t => t.Code)
                .ToTable("CALPROCEDURE");

            modelBuilder.Entity<Func>()
                .HasKey(t => t.FuncCode)
                .ToTable("FUNC");

            modelBuilder.Entity<Position>()
                .HasKey(t => t.PosCode)
                .ToTable("POSITION");

            modelBuilder.Entity<Setting>()
                .HasKey(t => t.Id)
                .ToTable("SETTINGS");
        }

        public DbSet<Person> Persons { get; set; }
        public DbSet<CalProcedure> CalProcedures { get; set; }
        public DbSet<Func> Funcs { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Setting> Settings { get; set; }
    }
}
