﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bitsharp.KeyCreator.Presentation
{
    public partial class frmKeyCreator : Form
    {
        public frmKeyCreator()
        {
            InitializeComponent();
            btnCopy.Enabled = false;
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            var key = KeyGenerator.KeyGenerator.Generate();

            lblKey.Text = key.ToUpper();
            btnCopy.Enabled = true;
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(lblKey.Text);
        }

        private void frmKeyCreator_Load(object sender, EventArgs e)
        {
            lblKey.ReadOnly = true;
            lblKey.BorderStyle = 0;
            lblKey.BackColor = this.BackColor;
            lblKey.TabStop = false;
        }
    }
}
