﻿using System;
using System.Net.Mail;
using System.Text;
using Bitsharp.Jamie.Logger;

namespace Bitsharp.Smtp.Smtp
{
    public class Smtp : ISmtp
    {
        private SmtpClient smtp;
        private readonly ILogger<Smtp> logger;

        public Smtp(ILogger<Smtp> logger)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));

            this.logger = logger;
        }

        public void Send(
            string @from, 
            string to, 
            string displayFrom, 
            string displayTo, 
            string subject, 
            string body, 
            string host, 
            string username,
            string password,
            int port,
            bool enableSsl = false)
        {
            try
            {
                smtp = new SmtpClient();
                MailMessage m = new MailMessage();
                m.From = new MailAddress(@from, displayFrom, Encoding.UTF8);
                m.To.Add(new MailAddress(to, displayTo, Encoding.UTF8));
                m.Subject = subject;
                m.Body = body;
                smtp.Host = host;
                smtp.Port = port;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential(username, password);
                smtp.EnableSsl = enableSsl; // runtime encrypt the SMTP communications using SSL
                smtp.Send(m);
            }
            catch (Exception ex)
            {
                var message = string.Format(
                    "Si è verificato un errore nell'invio della mail con questi dettagli. Host: [{0}], Username: [{1}], Password: [{2}]",
                    host, username, password);
               logger.LogError(message,ex);
            }
            
        }
    }
}
