﻿namespace Bitsharp.Smtp.Smtp
{
    public interface ISmtp
    {
        void Send(
            string from, 
            string to, 
            string displayFrom, 
            string displayTo,
            string subject, 
            string body, 
            string host, 
            string username, 
            string password,
            int port,
            bool enableSsl);
    }
}
