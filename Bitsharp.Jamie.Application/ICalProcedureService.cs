﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitsharp.Jamie.Application
{
    public interface ICalProcedureService
    {
        void SendMailForCalProcedureExpired(DateTime dateTimebefore);
    }
}
