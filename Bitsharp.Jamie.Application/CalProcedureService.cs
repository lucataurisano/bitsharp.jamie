﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bitsharp.Jamie.Application.Ports;
using Bitsharp.Jamie.Dal.Adapters;
using Bitsharp.Jamie.Logger;
using Bitsharp.Smtp.Smtp;

namespace Bitsharp.Jamie.Application
{
    public class CalProcedureService : ICalProcedureService
    {
        private readonly ILogger<CalProcedureService> logger;
        private readonly IQueryService queryService;
        private readonly ISmtp smtp;
        private readonly IJamieConfigurationManager configuration;

        public CalProcedureService(ILogger<CalProcedureService> logger, IQueryService queryService, ISmtp smtp, IJamieConfigurationManager configuration)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            if (queryService == null) throw new ArgumentNullException(nameof(queryService));
            if (smtp == null) throw new ArgumentNullException(nameof(smtp));
            if (configuration == null) throw new ArgumentNullException(nameof(configuration));

            this.logger = logger;
            this.queryService = queryService;
            this.smtp = smtp;
            this.configuration = configuration;
        }

        public void SendMailForCalProcedureExpired(DateTime dateTimebefore)
        {
            var message = string.Format("Starting get CalProcedureExpired at : [{0}]", DateTime.Now);
            logger.LogInfo(message);

            var host = configuration.GetJamieServiceMailSmtpHost();
            var username = configuration.GetJamieServiceMailUsernameHost();
            var password = configuration.GetJamieServiceMailPasswordHost();
            var port = configuration.GetJamieServiceMailPort();
            var from = configuration.GetJamieServiceMailFromHost();
            var fromDisplay = configuration.GetJamieServiceMailDisplayFromHost();
            var toDisplay = configuration.GetJamieServiceMailDisplayToHost();
            var body = configuration.GetJamieServiceMailBodyHost();
            var enableSSL = configuration.GetJamieServiceMailEnableSsl();

            SendMailForPositions(dateTimebefore, body, @from, fromDisplay, toDisplay, host, username, password, port,enableSSL);

            SendMailForProcedure(dateTimebefore, body, @from, fromDisplay, toDisplay, host, username, password, port, enableSSL);
        }

        // Posizioni principali - Root
        private void SendMailForPositions(DateTime dateTimebefore, string body, string @from, string fromDisplay,
            string toDisplay, string host, string username, string password, int port, bool enableSSL)
        {
            try
            {
                var results = queryService.CallProcedure()
                    .Join(queryService.Func(),
                        call => call.FuncCode,
                        func => func.FuncCode,
                        (call, func) => new {call, func})
                    .Join(queryService.Position(),
                        funcJoin => funcJoin.func.PosCode,
                        pos => pos.PosCode,
                        (funcJoin, pos) => new {funcJoin, pos})
                    .Join(queryService.Persons(),
                        posJoin => posJoin.pos.OwnerPersonCode,
                        pers => pers.Code,
                        (posJoin, pers) => new {posJoin.pos, posJoin.funcJoin.func, posJoin.funcJoin.call, pers,})
                    .Where(t => t.call.Duedate.HasValue && t.call.Duedate.Value > DateTime.Today &&
                                t.call.Duedate.Value < dateTimebefore)
                    .ToList();

                var subjectPosition = configuration.GetJamieServiceMailPositionMailSubject();
                var messageSubject = string.Format(subjectPosition,dateTimebefore.ToLongDateString());
                logger.LogInfo(messageSubject);
                logger.LogInfo(string.Format("Sono state trovate [{0}] posizioni associate", results.Count()));
                var procedureOwner = new Dictionary<string, string>();
                foreach (var expiredPosition in results)
                {
                    var mail = string.Empty;

                    mail = expiredPosition.pers.Email;
                    logger.LogInfo(string.Format("Procedure mail [{0}] ", mail));

                    bool mailExist = false;
                    if (!string.IsNullOrEmpty(mail))
                        mailExist = procedureOwner.ContainsKey(mail);
                    else
                        continue;

                    logger.LogInfo(string.Format("Mail esistente [{0}]", mailExist));
                    logger.LogInfo(string.Format("Mail [{0}]", expiredPosition.pers.Email));

                    if (!mailExist)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine(body
                            .Replace("{0}",
                                expiredPosition.pos.PosId + " - " + expiredPosition.pos.Name + " - " + 
                                expiredPosition.pos.Location)
                            .Replace("{1}", expiredPosition.call.Duedate.Value.ToLongDateString())
                            .Replace("{2}", expiredPosition.pers.Name));
                        procedureOwner.Add(mail, sb.ToString());
                        logger.LogInfo(string.Format("Aggiunta mail da inviare [{0}]", mail));
                    }
                    else if (mailExist)
                    {
                        string valueout = string.Empty;
                        procedureOwner.TryGetValue(mail, out valueout);
                        logger.LogInfo(string.Format("Valore: [{0}]", valueout));
                        StringBuilder sb = new StringBuilder(valueout);
                        sb.AppendLine(Environment.NewLine);
                        sb.AppendLine(body
                            .Replace("{0}",
                                expiredPosition.pos.PosId + " - " + expiredPosition.pos.Name + " - " +
                                expiredPosition.pos.Location)
                            .Replace("{1}", expiredPosition.call.Duedate.Value.ToLongDateString())
                            .Replace("{2}", expiredPosition.pers.Name));
                        procedureOwner.Remove(mail);
                        procedureOwner.Add(mail, sb.ToString());
                    }
                }

                foreach (KeyValuePair<string, string> keyValuePair in procedureOwner)
                {
                    smtp.Send(@from, keyValuePair.Key, fromDisplay, toDisplay, messageSubject, keyValuePair.Value, host,
                        username, password, port, enableSSL);
                }
            }
            catch (Exception ex)
            {
                logger.LogError("Si è verificato un errore durante la creazione delle mail in SendMailForCalProcedureExpired()",
                    ex);
            }
        }

        private void SendMailForProcedure(DateTime dateTimebefore, string body, string @from, string fromDisplay,
            string toDisplay, string host, string username, string password, int port, bool enableSSL)
        {
            try
            {
                var results = queryService.CallProcedure()
                    .Join(queryService.Func(),
                        call => call.FuncCode,
                        func => func.FuncCode,
                        (call, func) => new { call, func })
                    .Join(queryService.Position(),
                        funcJoin => funcJoin.func.PosCode,
                        pos => pos.PosCode,
                        (funcJoin, pos) => new { funcJoin, pos })
                    .Join(queryService.Persons(),
                        posJoin => posJoin.funcJoin.call.Assignedtocode,
                        pers => pers.Code,
                        (posJoinAssigned, persAssigned) => new { posJoinAssigned.pos, posJoinAssigned.funcJoin.func, posJoinAssigned.funcJoin.call,  persAssigned })
                    .Where(t => t.call.Duedate.HasValue && t.call.Duedate.Value > DateTime.Today &&
                                t.call.Duedate.Value < dateTimebefore)
                    .ToList();

                var subjectProcedure = configuration.GetJamieServiceMailProcedureMailSubject();
                var messageSubject = string.Format(subjectProcedure, dateTimebefore.ToLongDateString());
                logger.LogInfo(messageSubject);
                logger.LogInfo(string.Format("Sono state trovate [{0}] posizioni associate", results.Count()));
                var procedureOwner = new Dictionary<string, string>();
                foreach (var expiredPosition in results)
                {
                    var mail = string.Empty;

                    mail = expiredPosition.persAssigned.Email;
                    logger.LogInfo(string.Format("Procedure mail [{0}] ", mail));

                    bool mailExist = false;
                    if (!string.IsNullOrEmpty(mail))
                        mailExist = procedureOwner.ContainsKey(mail);
                    else
                        continue;

                    logger.LogInfo(string.Format("Mail esistente [{0}]", mailExist));
                    logger.LogInfo(string.Format("Mail [{0}]", expiredPosition.persAssigned.Email));

                    if (!mailExist)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine(body
                            .Replace("{0}",
                                expiredPosition.pos.PosId + " - " + expiredPosition.pos.Name + " - " +
                                expiredPosition.pos.Location)
                            .Replace("{1}", expiredPosition.call.Duedate.Value.ToLongDateString())
                            .Replace("{2}", expiredPosition.persAssigned.Name));
                        procedureOwner.Add(mail, sb.ToString());
                        logger.LogInfo(string.Format("Aggiunta mail da inviare [{0}]", mail));
                    }
                    else if (mailExist)
                    {
                        string valueout = string.Empty;
                        procedureOwner.TryGetValue(mail, out valueout);
                        logger.LogInfo(string.Format("Valore: [{0}]", valueout));
                        StringBuilder sb = new StringBuilder(valueout);
                        sb.AppendLine(Environment.NewLine);
                        sb.AppendLine(body
                            .Replace("{0}",
                                expiredPosition.pos.PosId + " - " + expiredPosition.pos.Name + " - " +
                                expiredPosition.pos.Location)
                            .Replace("{1}", expiredPosition.call.Duedate.Value.ToLongDateString())
                            .Replace("{2}", expiredPosition.persAssigned.Name));
                        procedureOwner.Remove(mail);
                        procedureOwner.Add(mail, sb.ToString());
                    }
                }

                foreach (KeyValuePair<string, string> keyValuePair in procedureOwner)
                {
                    smtp.Send(@from, keyValuePair.Key, fromDisplay, toDisplay, messageSubject, keyValuePair.Value, host,
                        username, password, port, enableSSL);
                }
            }
            catch (Exception ex)
            {
                logger.LogError("Si è verificato un errore durante la creazione delle mail in SendMailForCalProcedureExpired()",
                    ex);
            }
        }
    }
}
