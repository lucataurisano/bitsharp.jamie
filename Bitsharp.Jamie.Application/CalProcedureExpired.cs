﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitsharp.Jamie.Application
{
    public class CalProcedureExpired
    {
        public string CallProcedureName { get; set; }
        public DateTime CallProcedureExpiryDate { get; set; }
    }
}
