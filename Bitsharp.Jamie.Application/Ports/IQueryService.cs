﻿using System.Collections.Generic;
using System.Linq;
using Bitsharp.Jamie.Dal;
using Bitsharp.Jamie.Dal.DataModel;

namespace Bitsharp.Jamie.Application.Ports
{
    public interface IQueryService
    {
        IQueryable<Person> Persons();
        IQueryable<CalProcedure> CallProcedure();
        IQueryable<Func> Func();
        IQueryable<Position> Position();
    }
}
