﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Bitsharp.Jamie.Dal;
using Bitsharp.Jamie.Dal.DataModel;

namespace Bitsharp.Jamie.Application.Ports
{
    public class QueryService : IQueryService
    {
        private readonly DbContext context;

        public QueryService(DbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IQueryable<Person> Persons()
        {
            return ((JamieDbContext) context).Persons;
        }

        public IQueryable<CalProcedure> CallProcedure()
        {
            return ((JamieDbContext)context).CalProcedures;
        }

        public IQueryable<Func> Func()
        {
            return ((JamieDbContext)context).Funcs;
        }

        public IQueryable<Position> Position()
        {
            return ((JamieDbContext)context).Positions;
        }
    }
}
