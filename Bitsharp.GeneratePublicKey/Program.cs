﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bitsharp.GeneratePrivateKey
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inserire la chiave pubblica:");
            var s = Console.ReadLine();
            Guid g = new Guid(s);
            // -1341738540
            Console.WriteLine("Chiave privata: " + g.GetHashCode());
            Console.ReadLine();
        }
    }
}
